package Drivers;

import HGN.DAGRunner;
import HGN.HGNComposition;
import HGN.HGNRunner;
import Utilities.PatternFileReader;
import Utilities.RecallFileReader;
import Utilities.StoreFileReader;

import java.io.*;
import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

public class DriverHelper {

    public static ArrayList<Map.Entry<String,Double>> mapRecalledToLabels(final ArrayList<Map.Entry<Integer,Double>> recalled
            ,final ArrayList<String> labels)
    {
        ArrayList<Map.Entry<String,Double>> toReturn = new ArrayList<>(recalled.size());

        for(Map.Entry<Integer,Double> r : recalled)
        {
            // r.key -1 becuase HGN indexes start at 1 not 0
            toReturn.add(new AbstractMap.SimpleEntry<String, Double>(labels.get(r.getKey().intValue()-1),r.getValue()));
        }

        return  toReturn;
    }

    public static void writeCSVResults(final String filepath, ArrayList<ArrayList<Map.Entry<String,Double>>> results) throws IOException {
        File allRecallOut = new File(filepath+".allRecall");
        File outFile = new File(filepath);
        outFile.getParentFile().mkdirs();
        FileWriter fileWriter = null;
        BufferedWriter out = null;

        FileWriter allRecallWriter = null;
        BufferedWriter allRecallBuffered = null;
        try{
            fileWriter =new FileWriter(outFile,true);
            out = new BufferedWriter(fileWriter);

            allRecallWriter = new FileWriter(allRecallOut,true);
            allRecallBuffered = new BufferedWriter(allRecallWriter);

            for(ArrayList<Map.Entry<String,Double>> r : results) {
                if(r.size() > 0) {
                    out.write(r.get(0).getKey() + "\n");
                }else
                {
                    out.write("-1\n");
                }
            }

            for(ArrayList<Map.Entry<String,Double>> r : results)
            {
                for(Map.Entry<String,Double> e : r) {
                    allRecallBuffered.write(e.getKey()+" "+e.getValue()+"\n");
                }
            }
            allRecallBuffered.write("\n");

        }finally {
            out.close();
            fileWriter.close();

            allRecallBuffered.close();
            allRecallWriter.close();

        }
    }

    public static void writeResults(String filepath,ArrayList<ArrayList<Map.Entry<String,Double>>> results) throws IOException {
        File outFile = new File(filepath);
        outFile.getParentFile().mkdirs();

        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;
        try {
            fileOut =
                    new FileOutputStream(filepath+".backup");
            out = new ObjectOutputStream(fileOut);
            out.writeObject(results);
        }finally {
            out.close();
            fileOut.close();
        }
    }

    static int[] parseDimensions(String args)
    {
        int[] dim = null;

        ArrayList<Integer> dims = new ArrayList<Integer>();
        //get rid of < & >
        //args = args.substring(1, args.length()-1);

        //split on ,
        String[] nums = args.split(",");

        //convert str to ints
        for(String n : nums)
        {
            dims.add(Integer.parseInt(n));
        }
        dim = new int[dims.size()];
        for(int i=0;i<dims.size();i++)
        {
            dim[i]=dims.get(i);
        }
        return dim;
    }

    public static void saveLabels(final ArrayList<String> labels,String fileName) throws IOException {

        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;
        try {
            fileOut =
                    new FileOutputStream(fileName);
            out = new ObjectOutputStream(fileOut);
            out.writeObject(labels);
        }finally {
            out.close();
            fileOut.close();
        }

    }

    public static ArrayList<String> loadLabels(String fileName) throws IOException, ClassNotFoundException {

        FileInputStream fileIn = null;
        ObjectInputStream in = null;

        ArrayList<String> labels = null;

        try {
            fileIn = new FileInputStream(fileName);
            in = new ObjectInputStream(fileIn);

            labels = (ArrayList<String>) in.readObject();
        }finally {
            in.close();
            fileIn.close();
        }

        return labels;
    }

    /**
     *
     * @param runner
     * @param storageFile
     * @param dims
     * @return A ArrayList of labels
     * @throws IOException
     */
    public static ArrayList<String> storagePhase(HGNRunner runner, final String storageFile, final int[] dims) throws IOException
    {
        StoreFileReader reader = new StoreFileReader(storageFile,dims);
        runner.hgn.setState(HGNComposition.HGNState.Store);

        ArrayList<String> labels = new ArrayList<>();

        PatternFileReader.Pattern pattern = null;
        int totalStored = 0;
        while((pattern = reader.getNextPattern())!=null)
        {
            runner.run(pattern.patternDataPoints);
            totalStored++;
            System.out.println("Total Patterns stored: "+totalStored);
            labels.add(pattern.label);
        }

        return labels;
    }

    /**
     *
     * @param runner
     * @param storageFile
     * @param dims
     * @return A ArrayList of labels
     * @throws IOException
     */
    public static ArrayList<String> storagePhase(DAGRunner runner, final String storageFile, final int[] dims) throws IOException
    {
        StoreFileReader reader = new StoreFileReader(storageFile,dims);
        ArrayList<String> labels = new ArrayList<>();

        PatternFileReader.Pattern pattern = null;
        int totalStored = 0;
        while((pattern = reader.getNextPattern())!=null)
        {
            runner.run(pattern.patternDataPoints, HGNComposition.HGNState.Store);
            totalStored++;
            System.out.println("Total Patterns stored: "+totalStored);
            labels.add(pattern.label);
        }

        return labels;
    }

    /**
     *
     * @param runner
     * @param testFile
     * @param dims
     * @return ArrayList of result pattern indexes
     */
    public static ArrayList<ArrayList<Map.Entry<Integer,Double>>> recallPhase(HGNRunner runner,final String testFile,final int[] dims) throws IOException
    {
        RecallFileReader reader = new RecallFileReader(testFile,dims);
        runner.hgn.setState(HGNComposition.HGNState.Recall);

        ArrayList<ArrayList<Map.Entry<Integer,Double>>> results = new ArrayList<>();

        PatternFileReader.Pattern pattern = null;

        int totalTested = 0;
        while((pattern = reader.getNextPattern())!=null)
        {
            ArrayList<Map.Entry<Integer,Double>> r = runner.run(pattern.patternDataPoints).votes;
            totalTested++;
            System.out.println("Total Patterns tested: "+totalTested);
            results.add(r);
        }

        return results;
    }
}
