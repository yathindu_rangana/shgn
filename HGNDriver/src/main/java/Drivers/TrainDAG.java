package Drivers;

import HGN.DAGRunner;
import HGN.HGNRunner;
import HGN.HgnDAG;
import HGN.Serial.SerialHGNRunner;
import Utilities.DAGSerializer;
import Utilities.HGNSerializer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

public class TrainDAG {
    private static final Logger LOGGER = Logger.getLogger( TrainDAG.class.getName() );

    public static void printHelp()
    {
        System.out.println("Consumes a storage file, trains the HGN DAG and saves the trained bias arrays.\n"+
                "TrainHGN Usage:\n" +
                "java -jar TrainHGN 15,15 ../data/15x15_Store.txt ./state/15x15.state\n" +
                "\n"+
                "Breakdown of parameters:\n" +
                "java -jar HGN a,a b c d e\n" +
                "a = size of dimensions separated by ','\n" +
                "b = file path to starage patterns\n" +
                "c = file to load/save state from. This is optional, it doesn't exist it is created after the storage phase.\n" +
                "    If it is found the storage phase is skipped.");
    }


    public static void main(String[] args) {

        //get command line args
        int dim[] = null;
        int subnetDim[] = null;
        int dagBaseSize = 0;
        String memoryFilePattern = null;
        String saveFile = null;
        //parse args
        if(args.length != 3)
        {
            printHelp();
            System.exit(0);
        }
        else
        {
            dim = DriverHelper.parseDimensions(args[0]);
            dagBaseSize = dim[dim.length-1];
            subnetDim = Arrays.copyOfRange(dim,0,dim.length-1);
            memoryFilePattern = args[1];
            saveFile = args[2];
        }

        DAGRunner runner = null;
        ArrayList<String> labels = null;

        //if save file exist load
        if(new File(saveFile).exists()) {
            LOGGER.info("Found hgn state file. Skipping storage phase.");
        }else {
            //if save file doesn't exist
            //run storage phase
            runner = new DAGRunner(new HgnDAG(dagBaseSize,subnetDim));
            try {
                labels = DriverHelper.storagePhase(runner,memoryFilePattern,dim);
            }catch (IOException e)
            {
                LOGGER.severe("Failed to run storage phase: \n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
            //save state
            DAGSerializer saver = new DAGSerializer(saveFile);
            try {
                saver.save(runner.dag);
            }catch(IOException e)
            {
                LOGGER.severe("Failed to save: "+saveFile+"\n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
            //save labels
            try{
                DriverHelper.saveLabels(labels,saveFile+".labels");
            }catch (IOException e)
            {
                LOGGER.severe("Failed to save labels\n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
        }

    }


}
