package Drivers;

import HGN.DAGRunner;
import HGN.HGNComposition;
import HGN.HGNRunner;
import HGN.Serial.SerialHGNRunner;
import Utilities.DAGSerializer;
import Utilities.HGNSerializer;
import Utilities.PatternFileReader;
import Utilities.RecallFileReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;

public class TestDAG {

    private static final Logger LOGGER = Logger.getLogger( TestDAG.class.getName() );


    public static void printHelp()
    {
        System.out.println("Consumes a test file and a state file, runs the HGN against the test file and saves the results in a csv.\n"+
                "TestHGN Usage:\n" +
                "java -jar TestHGN 15,15 ../data/15x15_Test.txt ./state/15x15.state ./result/15x15.csv\n" +
                "\n"+
                "Breakdown of parameters:\n" +
                "java -jar TestHGN a,a b c d\n" +
                "a = size of dimensions separated by ','\n" +
                "b = file path to test patterns\n" +
                "c = file to load state from.\n" +
                "d = output csv file name");
    }


    public static void main(String[] args) {

        //get command line args
        int dim[] = null;
        int subnetDim[] = null;
        int dagBaseSize = 0;
        String testPatternFile = null;
        String saveFile = null;
        String outputCSVFile = null;
        //parse args
        if (args.length != 4) {
            printHelp();
            System.exit(0);
        } else {
            dim = DriverHelper.parseDimensions(args[0]);
            dagBaseSize = dim[dim.length-1];
            subnetDim = Arrays.copyOfRange(dim,0,dim.length-1);
            testPatternFile = args[1];
            saveFile = args[2];
            outputCSVFile = args[3];
        }

        DAGRunner runner = null;
        ArrayList<String> labels = null;

        //if save file exist load
        if (!new File(saveFile).exists()) {
            LOGGER.severe("State file not found. Aborting...\n" +
                    "File: "+saveFile);
            System.exit(-1);
        }

        DAGSerializer loader = new DAGSerializer(saveFile);
        try {
            LOGGER.info("Loading "+saveFile+".labels");
            labels = DriverHelper.loadLabels(saveFile + ".labels");
        }catch (IOException | ClassNotFoundException e)
        {
            LOGGER.severe("Failed to load: \n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }

        try {
            LOGGER.info("Loading Bias Arrays");
            runner = new DAGRunner(loader.load());
        }catch (IOException | ClassNotFoundException e)
        {
            LOGGER.severe("Failed to load: "+saveFile+"\n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }


        //run recall
        try {
            RecallFileReader reader = new RecallFileReader(testPatternFile,dim);
            PatternFileReader.Pattern pattern = null;

            long sumStoreTime = 0;
            int totalTested = 0;
            while((pattern = reader.getNextPattern())!=null)
            {
                ArrayList<ArrayList<Map.Entry<String,Double>>> results = new ArrayList<>();
                long start = System.nanoTime();
                ArrayList<Map.Entry<Integer,Double>> r = runner.run(pattern.patternDataPoints, HGNComposition.HGNState.Recall);
                long end = System.nanoTime();
                sumStoreTime+=(end-start);
                totalTested++;
                System.out.println("Total Patterns tested: "+totalTested);

                //map recalled index back to thier labels
                results.add(DriverHelper.mapRecalledToLabels(r,labels));
                //save resuts as csv
                try {
                    DriverHelper.writeCSVResults(outputCSVFile,results);
                } catch (IOException e) {
                    LOGGER.severe("failed to write csv!\n"+e);
                    LOGGER.severe("Attempting to save results as "+outputCSVFile+".backup");
                    try {
                        DriverHelper.writeResults(outputCSVFile,results);
                    } catch (IOException e1) {
                        LOGGER.severe("Backup save failed!\n"+e1);
                    }

                }
            }
            double avgStoreTimeNano=sumStoreTime/totalTested;
            LOGGER.info("Avg run time: "+avgStoreTimeNano);
        }catch(IOException e)
        {
            LOGGER.severe("Error in recall phase\n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }


        LOGGER.info("Finished running experiment. Closing down.");
    }


}
