package Drivers;

import HGN.HGNComposition;
import HGN.HGNRunner;
import HGN.Serial.SerialHGNRunner;
import Utilities.HGNSerializer;
import Utilities.PatternFileReader;
import Utilities.RecallFileReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

public class TestHGN {

    private static final Logger LOGGER = Logger.getLogger( TestHGN.class.getName() );


    public static void printHelp()
    {
        System.out.println("Consumes a test file and a state file, runs the HGN against the test file and saves the results in a csv.\n"+
                "TestHGN Usage:\n" +
                "java -jar TestHGN 15,15 ../data/15x15_Test.txt ./state/15x15.state ./result/15x15.csv\n" +
                "\n"+
                "Breakdown of parameters:\n" +
                "java -jar TestHGN a,a b c d\n" +
                "a = size of dimensions separated by ','\n" +
                "b = file path to test patterns\n" +
                "c = file to load state from.\n" +
                "d = output csv file name");
    }


    public static void main(String[] args) {

        //get command line args
        int dim[] = null;
        String testPatternFile = null;
        String saveFile = null;
        String outputCSVFile = null;
        //parse args
        if (args.length != 4) {
            printHelp();
            System.exit(0);
        } else {
            dim = DriverHelper.parseDimensions(args[0]);
            testPatternFile = args[1];
            saveFile = args[2];
            outputCSVFile = args[3];
        }

        HGNRunner runner = null;
        ArrayList<String> labels = null;

        //if save file exist load
        if (!new File(saveFile).exists()) {
            LOGGER.severe("State file not found. Aborting...\n" +
                    "File: "+saveFile);
            System.exit(-1);
        }

        HGNSerializer loader = new HGNSerializer(saveFile);
        try {
            labels = DriverHelper.loadLabels(saveFile + ".labels");
        }catch (IOException | ClassNotFoundException e)
        {
            LOGGER.severe("Failed to load: \n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }

        try {
            runner = new SerialHGNRunner(loader.loadHGN());
        }catch (IOException | ClassNotFoundException e)
        {
            LOGGER.severe("Failed to load: "+saveFile+"\n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }


        //run recall
        try {
            RecallFileReader reader = new RecallFileReader(testPatternFile,dim);
            runner.hgn.setState(HGNComposition.HGNState.Recall);

            PatternFileReader.Pattern pattern = null;

            int totalTested = 0;
            while((pattern = reader.getNextPattern())!=null)
            {
                ArrayList<ArrayList<Map.Entry<String,Double>>> results = new ArrayList<>();
                ArrayList<Map.Entry<Integer,Double>> r = runner.run(pattern.patternDataPoints).votes;
                totalTested++;
                System.out.println("Total Patterns tested: "+totalTested);

                //map recalled index back to thier labels
                results.add(DriverHelper.mapRecalledToLabels(r,labels));
                //save resuts as csv
                try {
                    DriverHelper.writeCSVResults(outputCSVFile,results);
                } catch (IOException e) {
                    LOGGER.severe("failed to write csv!\n"+e);
                    LOGGER.severe("Attempting to save results as "+outputCSVFile+".backup");
                    try {
                        DriverHelper.writeResults(outputCSVFile,results);
                    } catch (IOException e1) {
                        LOGGER.severe("Backup save failed!\n"+e1);
                    }

                }
            }
        }catch(IOException e)
        {
            LOGGER.severe("Error in recall phase\n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }

        LOGGER.info("Finished running experiment. Closing down.");
    }


}
