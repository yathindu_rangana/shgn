/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drivers;

import HGN.*;
import HGN.Serial.SerialHGNRunner;
import Utilities.HGNSerializer;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Yathindu Hettiarachchige
 */
public class Driver {

    private static final Logger LOGGER = Logger.getLogger( Driver.class.getName() );

    private static String[] generateInput(final int[] dims)
    {
        int prod=1;
        for(int d: dims)
            prod *= d;

        String[] str = new String[prod];
        for(int i = 0; i < prod; i++)
            str[i]="X";


        return str;
    }


    public static void printHelp()
    {
        System.out.println("HGN Usage:\n" +
                "java -jar HGN 15,15 ../data/15x15_Store.txt ../results/15_15_results.csv ../data/15x15_Test_exact.txt ./state/15x15.state\n" +
                "\n"+
                "Breakdown of parameters:\n" +
                "java -jar HGN a,a b c d e\n" +
                "a = size of dimensions separated by ','\n" +
                "b = file path to starage patterns\n" +
                "c = file pattern for csv output\n" +
                "d = file path to test file\n" +
                "e = file to load/save state from. This is optional, it doesn't exist it is created after the storage phase.\n" +
                "    If it is found the storage phase is skipped.");
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //get command line args
        int dim[] = null;
        String memoryFilePattern = null;
        String testFilePattern = null;
        String outputCSVFilePattern = null;
        String saveFile = null;
        //parse args
        if(args.length != 5)
        {
            printHelp();
            System.exit(0);
        }
        else
        {
            dim = DriverHelper.parseDimensions(args[0]);
            memoryFilePattern = args[1];
            outputCSVFilePattern = args[2];
            testFilePattern = args[3];
            saveFile = args[4];
        }

        HGNRunner runner = null;
        ArrayList<String> labels = null;

        //if save file exist load
        if(new File(saveFile).exists()) {
            LOGGER.info("Found hgn state file. Skipping storage phase.");
            //load hgn
            HGNSerializer loader = new HGNSerializer(saveFile);
            try {
                labels = DriverHelper.loadLabels(saveFile + ".labels");
            }catch (IOException | ClassNotFoundException e)
            {
                LOGGER.severe("Failed to load: \n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }

            try {
                runner = new SerialHGNRunner(loader.loadHGN());
            }catch (IOException | ClassNotFoundException e)
            {
                LOGGER.severe("Failed to load: "+saveFile+"\n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
        }else {
            //if save file doesn't exist
            //run storage phase
            runner = new SerialHGNRunner(dim);
            try {
                labels = DriverHelper.storagePhase(runner, memoryFilePattern,dim);
            }catch (IOException e)
            {
                LOGGER.severe("Failed to run storage phase: \n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
            //save state
            HGNSerializer saver = new HGNSerializer(saveFile);
            try {
                saver.save(runner.hgn);
            }catch(IOException e)
            {
                LOGGER.severe("Failed to save: "+saveFile+"\n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
            //save labels
            try{
                DriverHelper.saveLabels(labels,saveFile+".labels");
            }catch (IOException e)
            {
                LOGGER.severe("Failed to save labels\n"+e);
                LOGGER.severe("Exiting...");
                System.exit(-1);
            }
        }

        ArrayList<ArrayList<Map.Entry<Integer,Double>>> results = null;
        //run recall
        try {
            results = DriverHelper.recallPhase(runner, testFilePattern, dim);
        }catch(IOException e)
        {
            LOGGER.severe("Error in recall phase\n"+e);
            LOGGER.severe("Exiting...");
            System.exit(-1);
        }

        System.out.println("Results:");
        for(int i =0; i < results.size(); i++)
        {
            ArrayList<Map.Entry<Integer,Double>> r = results.get(i);
            System.out.println("Test "+(i+1)+":");
            for(Map.Entry<Integer,Double> e : r)
            {
                String lbl = "Not found";
                if(e.getKey().intValue() != -1) {
                    lbl = labels.get(e.getKey() - 1);
                }
                System.out.println("Label " + lbl+ " " + e.getValue());
            }
        }

        ArrayList<ArrayList<Map.Entry<String,Double>>> mappedResults = new ArrayList<>(results.size());
        for(ArrayList<Map.Entry<Integer,Double>> r : results)
        {
            mappedResults.add(DriverHelper.mapRecalledToLabels(r,labels));
        }
        //save resuts as csv
        try {
            DriverHelper.writeCSVResults(outputCSVFilePattern,mappedResults);
        } catch (IOException e) {
            LOGGER.severe("failed to write csv!\n"+e);
            LOGGER.severe("Attempting to save results as "+outputCSVFilePattern+".backup");
            try {
                DriverHelper.writeResults(outputCSVFilePattern,mappedResults);
            } catch (IOException e1) {
                LOGGER.severe("Backup save failed!\n"+e1);
            }

        }

        LOGGER.info("Finished running experiment. Closing down.");
    }
    
}
