Hierarchical Graph Neuron (HGN)
==============

HGN is an associative memory which can be used as pattern recognizer.
This implementation of HGN supports:

- Multiple dimensions
- Both serial and parallel (**currently incomplete**) HGN implementations.
- Once the storage (training) phase is done the HGN's bias arrays are serialized so that
they can be loaded in later. 

Project Structure
--------------
The project is structured as follows:

- `src` : The source code
  - `main` : The main HGN code
    - `java`
      - `HGN` : HGN logic and main class
        - `Parallel` : Parallel implementation code
        - `Serial` : Serial implementation code
      - `Utilities`
  - `test` : All test code
- `data` : The sample data files
- `HGN_jar` : The built jar file is here

Program Usage
--------------

The `HGN_jar` directory contains three compiled jar files.

### HGN.jar
HGN.jar file is useful for running small tests 

    cd HGN_jar
    java -jar ./HGN.jar 15,15 ../data/15x15_Store.txt ../results/15_15_results.csv ../data/15x15_Test_exact.txt ./state/15x15.state

This command runs a HGN with the these options:

- HGN with 15x15 pattern.
- The store file is `data/15x15_Store.txt`
- The recalled patterns will be stored in `results/15_15_results.csv`. If the `results` directory doesn't exist, it will be created.
- Test pattern file is `data/15x15_Test_exact.txt`
- The bias arrays produced by the storage phase will be stored in `state/15x15.state`. If the `state` directory doesn't exist it will be created.

### TestHGN.jar
TestHGN.jar consumes a test file and a state file, runs the HGN against the test file and saves the results in a csv.
    
    java -jar TestHGN 15,15 ../data/15x15_Test.txt ./state/15x15.state ./result/15x15.csv

Breakdown of parameters:

- HGN with 15x15 pattern
- The test file is `../data/15x15_Test.txt`
- Bias arrays are loaded from `./state/15x15.state`
- The results are stored in `./result/15x15.csv`

### TrainHGN.jar
TrainHGN.jar consumes a storage file, trains the HGN and saves the trained bias arrays

    java -jar TrainHGN 15,15 ../data/15x15_Store.txt ./state/15x15.state

Breakdown of arguments:

- HGN with 15x15 pattern 
- The storage file is `../data/15x15_Store.txt`
- After training the bias arrays are stored in `./state/15x15.state`

Software Architecture
--------------
There are three main entities in HGN. The individual graph neurons `GN.java`, the GNs arranged into a hierarchy `HGNComposition.java`
and class that handles actually running the HGN `HGNRunner.java`. 

`GN` and `HGNComposition` are data structures, they carry out no actual HGN functions. A `HGNCompositon` is associated with a
`HGNRunner`. The `HGNRunner` handles the actual HGN functionality such as mapping inputs to neurons and swapping neighbouring symbols.
This seperation between data and data manipulation allows flexibility in the way we manipulate HGN. 

`HGNRunner` is abstract, a concrete implementation of `HGNRunner` is `SerialHGNRunner` which runs the HGN serially.
Or `ParallelHGNRunner` which runs HGN in parallel.   
    
TODO List
------------
The following features are missing or incomplete:

- Parallel HGNRunner
- Creating networks of multiple HGN compositions. 
- Profiling shows that  SerialRunner
