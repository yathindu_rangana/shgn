import HGN.HGNComposition;
import HGN.HgnDAG;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class HgnDAGTest {
    @Test
    public void testHgnDAGInitializationValid(){

        //test a bunch of valid cases
        ArrayList<int[]> dimSize = new ArrayList<>();
        ArrayList<Integer> dagNetBaseSize = new ArrayList<>();
        dimSize.add(new int[]{5});
        dagNetBaseSize.add(3);
        dimSize.add(new int[]{5,5});
        dagNetBaseSize.add(2);
        dimSize.add(new int[]{5,5,5});
        dagNetBaseSize.add(7);
        dimSize.add(new int[]{31,31,31});
        dagNetBaseSize.add(9);
        dimSize.add(new int[]{51,51,51});
        dagNetBaseSize.add(11);

        Assert.assertEquals("Test set up incorret!",dagNetBaseSize.size(),dimSize.size());

        for(int i =0; i < dagNetBaseSize.size(); i++) {
            HgnDAG dag = new HgnDAG(dagNetBaseSize.get(i),dimSize.get(i));

            Assert.assertEquals(dag.nodes.size(),dagNetBaseSize.get(i).intValue());
            Assert.assertTrue(dag.top != null);
        }
    }

}
