import HGN.HGNComposition;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class HGNCompositionTest
{
    @Test
    public void testInitializationInvalid()
    {
        //test a bunch of valid cases
        ArrayList<int[]> dimSize = new ArrayList<>();
        dimSize.add(new int[]{4});
        dimSize.add(new int[]{5,2});
        dimSize.add(new int[]{5,58,5});
        dimSize.add(new int[]{1});
        dimSize.add(new int[]{51,90,51});


        for(int[] d : dimSize)
        {
            try {
                HGNComposition hgn = new HGNComposition(d);
                String msg = "Invalid input dimensions passed!\n";
                for(int i : d)
                    msg+=i+"\n";
                Assert.fail(msg);
            }catch(IllegalArgumentException e)
            {
            }
        }
    }

    @Test
    public void testInitializationValid()
    {
        //test a bunch of valid cases
        ArrayList<int[]> dimSize = new ArrayList<>();
        dimSize.add(new int[]{5});
        dimSize.add(new int[]{5,5});
        dimSize.add(new int[]{5,5,5});
        dimSize.add(new int[]{31,31,31});
        dimSize.add(new int[]{51,51,51});


        for(int[] d : dimSize)
        {
            HGNComposition hgn = new HGNComposition(d);

            int[] dSize = d.clone();

            for(ArrayList l: hgn.getLayers())
            {
                int dSizeProd = 1;
                for(int j : dSize)
                    dSizeProd *= j;
                //compare total gns in each layer
                Assert.assertEquals(dSizeProd,l.size());

                for(int j=0;j < dSize.length;j++) {
                    if(dSize[j]>1)
                        dSize[j] = dSize[j] - 2;
                }
            }
        }
    }
}
