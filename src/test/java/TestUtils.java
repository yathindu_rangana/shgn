public class TestUtils {
    public static int[] toIntArray(char[] in)
    {
        int[] out = new int[in.length];

        for(int i =0; i < in.length; i++)
        {
            out[i]=(int)in[i];
        }

        return out;
    }
}
