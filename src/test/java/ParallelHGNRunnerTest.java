import org.junit.Assert;
import org.junit.Test;
import HGN.Parallel.ParallelHGNRunner;

import java.util.ArrayList;
import java.util.Arrays;

public class ParallelHGNRunnerTest {
    @Test
    public void convertToIndex_3D() throws Exception {
        int[] dSize = new int[] {3,3,3};
        ArrayList<int[]> coords = new ArrayList<>();
        ArrayList<Integer> indexes = new ArrayList<>();

        coords.add( new int[]{0,0,0});
        indexes.add(0);

        coords.add(new int[]{1,0,0});
        indexes.add(1);

        coords.add(new int[]{2,0,0});
        indexes.add(2);

        coords.add(new int[]{0,1,0});
        indexes.add(3);

        coords.add(new int[]{1,1,0});
        indexes.add(4);

        coords.add(new int[]{2,1,0});
        indexes.add(5);

        coords.add(new int[]{0,2,0});
        indexes.add(6);

        coords.add(new int[]{1,2,0});
        indexes.add(7);

        coords.add(new int[]{2,2,0});
        indexes.add(8);

        coords.add(new int[]{0,0,1});
        indexes.add(9);

        coords.add(new int[]{1,0,1});
        indexes.add(10);

        coords.add(new int[]{1,2,2});
        indexes.add(25);

        coords.add(new int[]{2,2,2});
        indexes.add(26);

        ParallelHGNRunner runner = new ParallelHGNRunner(dSize,4);

        for(int i = 0; i<coords.size(); i++) {
            int idx = runner.convertToIndex(coords.get(i), dSize);

            Assert.assertEquals(indexes.get(i).intValue(), idx);
        }
    }

    @Test
    public void convertToIndex_1D() throws Exception {
        int[] dSize = new int[] {3};
        ArrayList<int[]> coords = new ArrayList<>();
        ArrayList<Integer> indexes = new ArrayList<>();

        coords.add( new int[]{0});
        indexes.add(0);

        coords.add(new int[]{1});
        indexes.add(1);

        coords.add(new int[]{2});
        indexes.add(2);

        ParallelHGNRunner runner = new ParallelHGNRunner(dSize,4);

        for(int i = 0; i<coords.size(); i++) {
            int idx = runner.convertToIndex(coords.get(i), dSize);

            Assert.assertEquals(indexes.get(i).intValue(), idx);
        }
    }

    @Test
    public void getNeighbours1D()
    {
        int[] dSize = new int[] {3};
        ParallelHGNRunner runner = new ParallelHGNRunner(dSize,4);

        ArrayList<int[]> input = new ArrayList<>();
        ArrayList<ArrayList<int[]>> output = new ArrayList<>();

        input.add(new int[]{0});
        output.add(new ArrayList<>(Arrays.asList(new int[]{1},null)));

        input.add(new int[]{1});
        output.add(new ArrayList<>(Arrays.asList(new int[]{2},new int[]{0})));

        input.add(new int[]{2});
        output.add(new ArrayList<>(Arrays.asList(null,new int[]{1})));

        for(int i=0; i < input.size();i++)
        {
            ArrayList<int[]> result = runner.getNeighbours(input.get(i),dSize);
            Assert.assertEquals(output.get(i).size(),result.size());
            for(int r=0; r < result.size();r++)
            {
                Assert.assertArrayEquals(output.get(i).get(r),result.get(r));
            }
        }

    }

}