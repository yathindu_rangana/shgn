import HGN.HGNComposition;
import org.junit.Assert;
import org.junit.Test;
import HGN.Serial.SerialHGNRunner;

import java.util.*;

import static org.junit.Assert.*;

public class SerialHGNRunnerTest {

    @Test
    public void testHash()
    {
        List<Integer> s1  = new Vector<>();
        s1.add(1);
        s1.add(2);
        s1.add(-1);
        s1.add(10);

        List<Integer> s2  = new Vector<>();
        s2.add(1);
        s2.add(2);
        s2.add(-1);
        s2.add(10);

        List<Integer> s3  = new Vector<>();
        s3.add(2);
        s3.add(1);
        s3.add(10);
        s3.add(-1);

        List<Integer> s4  = new Vector<>();
        s4.add(10);
        s4.add(-1);
        s4.add(2);
        s4.add(1);

        int hash1  = s1.hashCode();
        int hash2  = s2.hashCode();
        int hash3  = s3.hashCode();
        int hash4  = s4.hashCode();

        Assert.assertEquals(hash1,hash2);
        Assert.assertNotEquals(hash3,hash2);

    }



    @Test
    public void run1D() throws Exception {
        final int[] dSize = new int[] {5};
        SerialHGNRunner runner = new SerialHGNRunner(dSize);

        ArrayList<Integer> expectedRecall = new ArrayList<>();
        ArrayList<char[]> storeInputs = new ArrayList<>();
        storeInputs.add(new char[]{'X','O','X','X','O'});
        expectedRecall.add(1);
        storeInputs.add(new char[]{'O','O','X','X','O'});
        expectedRecall.add(2);
        storeInputs.add(new char[]{'X','O','O','O','O'});
        expectedRecall.add(3);

        ArrayList<char[]> recallInputs = new ArrayList<>();
        recallInputs.add(new char[]{'O','O','O','O','O'});
        expectedRecall.add(2);
        recallInputs.add(new char[]{'O','X','X','X','O'});
        expectedRecall.add(1);
        recallInputs.add(new char[]{'O','X','O','O','O'});
        expectedRecall.add(2);

        double avgStoreTimeNano =0;
        long sumStoreTime = 0;
        for(final char[] in : storeInputs)
        {
            long start = System.nanoTime();
            runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Store);
            long end = System.nanoTime();
            sumStoreTime+=(end-start);
        }
        avgStoreTimeNano=sumStoreTime/storeInputs.size();
        System.out.println("Avg storage time for 1D-5 HGNComposition: "+avgStoreTimeNano);


        ArrayList<Integer> results = new ArrayList<>();
        //test true positive
        for(final char[] in: storeInputs)
        {
            ArrayList<Map.Entry<Integer,Double>> r  = runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Recall).votes;
            results.add(r.get(0).getKey());
        }

        //test true negatives
        for(final char[] in: recallInputs)
        {
            ArrayList<Map.Entry<Integer,Double>> r  = runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Recall).votes;
            results.add(r.get(0).getKey());
        }

        Assert.assertEquals(results.size(),expectedRecall.size());
        for(int r=0; r < results.size();r++)
        {
            assertEquals(expectedRecall.get(r),results.get(r));
        }
    }

}