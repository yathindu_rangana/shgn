import HGN.DAGRunner;
import HGN.HGNComposition;
import HGN.HgnDAG;
import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class DAGRunnerTest {

    @Test
    public void FuzzDAGTest(){
        ArrayList<Integer> expectedRecall = new ArrayList<>();
        ArrayList<char[]> storeInputs = new ArrayList<>();
        storeInputs.add(new char[]{'X','O','X','X','O','X','O','X','X','O','X','O','X','X','O'});
        storeInputs.add(new char[]{'O','O','X','X','O','O','O','X','X','O','O','O','X','X','O'});
        storeInputs.add(new char[]{'X','X','O','O','X','X','O','O','O','O','X','X','O','O','O'});

        ArrayList<char[]> fuzzyInputs = new ArrayList<>();
        fuzzyInputs.add(new char[]{'X','O','O','X','O','X','O','X','X','O','X','O','X','X','O'});
        expectedRecall.add(1);
        fuzzyInputs.add(new char[]{'O','O','O','O','O','O','O','X','X','O','O','O','X','X','O'});
        expectedRecall.add(2);
        fuzzyInputs.add(new char[]{'X','O','O','O','O','X','O','O','O','O','X','X','O','O','O'});
        expectedRecall.add(3);

        final int[] dSize = new int[] {5};
        HgnDAG dag =  new HgnDAG(3,dSize);
        DAGRunner runner  = new DAGRunner(dag);

        double avgStoreTimeNano =0;
        long sumStoreTime = 0;
        for(final char[] in : storeInputs)
        {
            long start = System.nanoTime();
            runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Store);
            long end = System.nanoTime();
            sumStoreTime+=(end-start);
        }
        avgStoreTimeNano=sumStoreTime/storeInputs.size();
        System.out.println("Avg storage time for 1D-15 DAG HGNComposition: "+avgStoreTimeNano);


        ArrayList<Integer> results = new ArrayList<>();
        for(final char[] in: fuzzyInputs)
        {
            ArrayList<Map.Entry<Integer,Double>> r  = runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Recall);
            results.add(r.get(0).getKey());
        }

        Assert.assertEquals(results.size(),expectedRecall.size());
        for(int r=0; r < results.size();r++)
        {
            assertEquals(expectedRecall.get(r),results.get(r));
        }

    }

    @Test
    public void SimpleDAGTest(){
        ArrayList<Integer> expectedRecall = new ArrayList<>();
        ArrayList<char[]> storeInputs = new ArrayList<>();
        storeInputs.add(new char[]{'X','O','X','X','O','X','O','X','X','O','X','O','X','X','O'});
        expectedRecall.add(1);
        storeInputs.add(new char[]{'O','O','X','X','O','O','O','X','X','O','O','O','X','X','O'});
        expectedRecall.add(2);
        storeInputs.add(new char[]{'X','O','O','O','O','X','O','O','O','O','X','O','O','O','O'});
        expectedRecall.add(3);

        final int[] dSize = new int[] {5};
        HgnDAG dag =  new HgnDAG(3,dSize);
        DAGRunner runner  = new DAGRunner(dag);

        double avgStoreTimeNano =0;
        long sumStoreTime = 0;
        for(final char[] in : storeInputs)
        {
            long start = System.nanoTime();
            runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Store);
            long end = System.nanoTime();
            sumStoreTime+=(end-start);
        }
        avgStoreTimeNano=sumStoreTime/storeInputs.size();
        System.out.println("Avg storage time for 1D-15 DAG HGNComposition: "+avgStoreTimeNano);

        ArrayList<Integer> results = new ArrayList<>();
        //test true positive
        for(final char[] in: storeInputs)
        {
            ArrayList<Map.Entry<Integer,Double>> r  = runner.run(TestUtils.toIntArray(in), HGNComposition.HGNState.Recall);
            results.add(r.get(0).getKey());
        }

        Assert.assertEquals(results.size(),expectedRecall.size());
        for(int r=0; r < results.size();r++)
        {
            assertEquals(expectedRecall.get(r),results.get(r));
        }
    }


}
