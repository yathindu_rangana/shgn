package Utilities;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

public class StoreFileReader extends PatternFileReader{

    private static final java.util.logging.Logger LOGGER = Logger.getLogger( StoreFileReader.class.getName() );

    public StoreFileReader(String fileName,final int[] dims) throws FileNotFoundException {
        super(fileName,dims);
    }


    @Override
    protected Pattern readPatternSeperator() throws IOException {
        Pattern pattern= null;

        //read 'S' command
        String command = bufferedReader.readLine();
        //read label
        String label = bufferedReader.readLine();

        //validate
        if(command != null &&  !command.equals("S"))
        {
            throw new IOException("Command expected S but got "+command);
        }

        if(label != null && label.length() == 0)
        {
            throw new IOException("Label empty or null");
        }

        if(command != null || label != null) {
            pattern = new Pattern();
            pattern.command = command.charAt(0);
            pattern.label = label;
        }
        return pattern;
    }
}
