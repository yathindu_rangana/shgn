package Utilities;

import HGN.HGNComposition;

import java.util.ArrayList;

public class DebugUtils {

    public static void printHGN(final ArrayList<ArrayList<Integer>> resultsPerLayer)
    {

        for(int layer=0; layer < resultsPerLayer.size(); layer++) {
            if(resultsPerLayer.get(layer).size() <= 15) {
                for (int space = 0; space < 5 - (resultsPerLayer.size() -layer); space++)
                    System.out.print("   ");
                for (Integer r : resultsPerLayer.get(layer)) {
                    String s = r.intValue() == -1 ? "-" : r.toString();
                    System.out.print(" " + s + " ");
                }
                System.out.print("\n");
            }
        }
    }
}
