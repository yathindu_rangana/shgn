package Utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

public abstract class PatternFileReader {

    public class Pattern
    {
        public char command;
        public String label;
        public int[] patternDataPoints;

        public Pattern()
        {
            patternDataPoints = new int[numDataPoints];
        }
    }

    private static final java.util.logging.Logger LOGGER = Logger.getLogger( PatternFileReader.class.getName() );
    protected BufferedReader bufferedReader;
    protected final int[] dims;
    protected final int numDataPoints;

    public PatternFileReader(String fileName,final int[] dims) throws FileNotFoundException {
        try {
            FileReader fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
        }
        catch (FileNotFoundException e)
        {
            LOGGER.severe("Failed to open: "+fileName);
            throw e;
        }
        this.dims = dims;
        int patternSize = 1;
        for(int d : this.dims)
            patternSize *=d;
        this.numDataPoints = patternSize;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(bufferedReader != null)
            bufferedReader.close();
    }

    protected abstract Pattern readPatternSeperator() throws IOException;

    public Pattern getNextPattern() throws IOException
    {
        Pattern p =readPatternSeperator();

        if(p != null) {
            int i = 0;
            while (i < this.numDataPoints) {
                char c = (char) bufferedReader.read();
                if(c != '\n') {
                    p.patternDataPoints[i] = c;
                    i++;
                }
            }
            bufferedReader.readLine();
        }
        return p;
    }

}
