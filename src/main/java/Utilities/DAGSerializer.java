package Utilities;

import HGN.HgnDAG;
import java.io.*;

public class DAGSerializer {

    private String fileName;
    public DAGSerializer(String fileName)
    {
        this.fileName=fileName;
    }

    public void save(HgnDAG dag) throws IOException
    {
        new File(fileName).getParentFile().mkdirs();

        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;

        try {
            fileOut =
                    new FileOutputStream(this.fileName);
            out = new ObjectOutputStream(fileOut);
            out.writeObject(dag);
        }finally {
            out.close();
            fileOut.close();
        }
    }

    public HgnDAG load() throws IOException, ClassNotFoundException
    {
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        HgnDAG dag;

        try{
            fileIn = new FileInputStream(this.fileName);
            in = new ObjectInputStream(fileIn);
            dag = (HgnDAG) in.readObject();
        }finally {
            in.close();
            fileIn.close();
        }
        return dag;
    }


}
