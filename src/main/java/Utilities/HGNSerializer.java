package Utilities;

import HGN.HGNComposition;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;

public class HGNSerializer {

    private String fileName;
    public HGNSerializer(String fileName)
    {
         this.fileName=fileName;
    }

    public void save(HGNComposition hgn) throws IOException
    {
        new File(fileName).getParentFile().mkdirs();

        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;

        try {
            fileOut =
                    new FileOutputStream(this.fileName);
            out = new ObjectOutputStream(fileOut);
            out.writeObject(hgn);
        }finally {
            out.close();
            fileOut.close();
        }
    }

    public HGNComposition loadHGN() throws IOException, ClassNotFoundException {
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        HGNComposition hgn;

        try{
            fileIn = new FileInputStream(this.fileName);
            in = new ObjectInputStream(fileIn);
            hgn = (HGNComposition) in.readObject();
        }finally {
            in.close();
            fileIn.close();
        }
        return hgn;
    }

}
