package Utilities;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileNotFoundException;
import java.io.IOException;

public class RecallFileReader extends PatternFileReader {

    public RecallFileReader(String fileName,final int[] dims) throws FileNotFoundException {
        super(fileName, dims);
    }

    @Override
    protected Pattern readPatternSeperator() throws IOException {
        String command = bufferedReader.readLine();

        Pattern pattern = null;

        if(command !=null && (!command.equals("R") || command.length() != 1))
        {
            throw new IOException("Command expected R but got "+command);
        }

        if(command != null) {
            pattern = new Pattern();
            pattern.command = command.charAt(0);
        }
        return pattern;
    }

}
