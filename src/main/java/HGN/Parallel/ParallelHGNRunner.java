package HGN.Parallel;

import HGN.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Parallel HGNRunner implements the run method from HGNRunner in a parallel manner.
 *  This class is incomplete. The basic idea is to has a fixed number of threads available to the runner.
 *  These threads are used to run parallel operations on the HGN structure.
 *  The HGN algorithm can be split into 2 phases these two:
 *      > Receive input and share with neighbours (wait for neighbours values)
 *      > Compute bias array entry and send to upper neuron
 *  These two phases can be implemented as Runnables and run via the thread pool. Things like 'futures' can be used to
 *  synchronize.
 */
public class ParallelHGNRunner extends HGNRunner {

    private static final Logger LOGGER = Logger.getLogger( ParallelHGNRunner.class.getName() );
    ExecutorService excutor;

    public ParallelHGNRunner( final int[] dims, int parallelFactor)
    {
        super(dims);
        final int numThreads = Runtime.getRuntime().availableProcessors()*parallelFactor;
        LOGGER.info("Using "+numThreads+" threads");
        excutor = Executors.newFixedThreadPool(numThreads);
    }

    public ParallelHGNRunner(HGNComposition hgn) {
        super(hgn);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        excutor.shutdown();
    }

    @Override
    public HGNResult run(final int[] input)
    {
        throw new NotImplementedException();
    }


}
