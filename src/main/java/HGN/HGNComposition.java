package HGN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * The HGNComposition represents a collection of GNs.
 * It creates and manages the hierarchy.
 */
public class HGNComposition implements Serializable {

    public enum HGNState
    {
        Store,
        Recall
    }

    private ArrayList<ArrayList<GN>> layers;
    private final int[] patternSize;
    private final int numLayers;
    private HGNState state;
    private int numBaseInputs;

    //diagnostic info
    private ArrayList<int[]> diagnostic_layerSizes;

    public HGNComposition(ArrayList<ArrayList<GN>> layers, ArrayList<int[]> layerSizes)throws IllegalArgumentException{
        state = HGNState.Store;
        patternSize = layerSizes.get(0);
        numLayers = layers.size();

        //validate dim size are odd
        for(int i : patternSize)
        {
            if((i%2) == 0)
                throw new IllegalArgumentException ("Dimension sizes need to be odd! ");
        }
        if(patternSize.length == 1 && patternSize[0]==1)
            throw new IllegalArgumentException ("Dimension sizes need to be odd! ");

        this.layers = layers;
        this.numBaseInputs = layers.get(0).size();

    }

    public HGNComposition(int[] basePatternSize) throws IllegalArgumentException
    {
        state = HGNState.Store;
        patternSize = basePatternSize;

        //validate dim size are odd
        for(int i : patternSize)
        {
            if((i%2) == 0)
                throw new IllegalArgumentException ("Dimension sizes need to be odd! ");
        }
        if(patternSize.length == 1 && patternSize[0]==1)
            throw new IllegalArgumentException ("Dimension sizes need to be odd! ");

        numLayers = calcNumLayers();

        //initialized arraylists for each layer
        initLayers();

        this.numBaseInputs = 1;
        for(int i : basePatternSize)
            this.numBaseInputs *=i;

    }

    public int getNumBaseInputs()
    {
        return this.numBaseInputs;
    }

    public void setState(HGNState newState){
        state=newState;
    }

    public HGNState getState() {
        return state;
    }

    public String diagnosticSummery()
    {
        String msg= "Hgn summery:\n";

        for(int i =0; i<diagnostic_layerSizes.size();i++)
        {
            msg+="Level "+(i+1)+":\n";
            int[] dims = diagnostic_layerSizes.get(i);
            for(int d : dims)
                msg+=d+" ";
            msg+=" = "+layers.get(i).size();
            msg+="\n";
        }
        return msg;
    }


    /**
     *
     * @return The maximum number of gn layers required
     */
    private int calcNumLayers()
    {
        //get the largest dimension
        int maxDim = Arrays.stream(patternSize).max().getAsInt();
        return maxDim - (maxDim/2);
    }

    private void initLayers()
    {
        this.layers = new ArrayList<>(numLayers);
        this.diagnostic_layerSizes = new ArrayList<>(numLayers);
        int[] currSize = patternSize.clone();
        for(int layer = 0; layer < numLayers; layer++)
        {
            //add diagnostic info
            diagnostic_layerSizes.add(layer,currSize.clone());

            //calculate total GN in layer
            int totalGNs = 1;
            for(int j : currSize)
                totalGNs *= j;

            //make arraylist to store layer
            this.layers.add(new ArrayList<>(totalGNs));

            //init new GNs in layer
            for(int gn=0; gn < totalGNs; gn++) {
                layers.get(layer).add(new GN(currSize.length));
            }

            //update currSize
            for(int i=0; i < currSize.length; i++)
            {
                if(currSize[i] > 1)
                    currSize[i] = currSize[i]-2;
            }
        }
    }

    public ArrayList<ArrayList<GN>> getLayers() {
        return layers;
    }

    public ArrayList<int[]> getDiagnostic_layerSizes() {
        return diagnostic_layerSizes;
    }
}
