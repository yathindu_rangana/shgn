package HGN;

import sun.util.logging.PlatformLogger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * HGNRunner handles the main logic of HGN such as managing the input to neuron mapping, neighbouring symbol swapping
 * and managing the voting process.
 *
 * HGNRunner handles all interaction between neurons. Since HGnRunner is abstract differant child classes can choose to
 * handle these neuron to neuron interactions differently while still taking advantage of the HGNComposition data structure.
 */
public abstract class HGNRunner {

    public class HGNResult{
        public ArrayList<Map.Entry<Integer,Double>> votes;
        public int topResult;

        public HGNResult(ArrayList<Map.Entry<Integer,Double>> votes, int topResult)
        {
            this.votes = votes;
            this.topResult = topResult;
        }

    }


    private static final Logger LOGGER = Logger.getLogger( HGNRunner.class.getName() );
    public HGNComposition hgn;

    public HGNRunner(final int[] dims)
    {
        hgn = new HGNComposition(dims);
        LOGGER.info(hgn.diagnosticSummery());
    }

    public HGNRunner(HGNComposition hgn)
    {
        this.hgn = hgn;
    }

    /**
     *
     * @param input The pattern
     * @param state which state to run in recall or store
     * @return An array list of Entries. Key = pattern index. Value = score
     */
    public HGNResult run(final int[] input, HGNComposition.HGNState state)
    {
        hgn.setState(state);
        return run(input);
    }

    /**
     *
     * @param input The pattern
     * @return An array list of Entries. Key = pattern index. Value = score
     */
    public abstract HGNResult run(final int[] input);

    //column major
    public int convertToIndex(final int[] multiDimCoord,final int[] dimSize)
    {
        if(multiDimCoord.length != dimSize.length)
            throw new IllegalArgumentException("Index cardinality didn't match!");

        int ret = 0;
        int count = 0;
        while(count < dimSize.length)
        {

            int prod = 1;
            for(int i =0; i < count; i++)
            {
                prod *=dimSize[i];
            }
            ret += prod*multiDimCoord[count];

            count++;
        }

        return ret;
    }

    public ArrayList<int[]> getNeighbours(final int[] coord, final int[] dims) {
        ArrayList<int[]> ret = new ArrayList<>();
        //initialize the arrayList with enough space for both neighbours from each dimension
        while(ret.size() < (coord.length*2))
            ret.add(null);

        for(int i =0; i < coord.length; i++)
        {
            int[] copy = coord.clone();
            copy[i] += 1;
            if(isCoordValid(copy,dims))
                ret.set(i*2,copy.clone());

            copy = coord.clone();
            copy[i] -= 1;
            if(isCoordValid(copy,dims))
                ret.set((i*2)+1,copy.clone());
        }

        return ret;
    }

    protected boolean isCoordValid(final int[] coord, final int[] dims){
        if(coord.length != dims.length)
            throw new IllegalArgumentException("Coordinate cardinality didn't match!");
        for(int i =0; i < coord.length;i++)
        {
            if(coord[i] < 0 || coord[i] >= dims[i])
                return false;
        }
        return true;
    }
}
