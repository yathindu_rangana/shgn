package HGN;

import com.sun.javaws.exceptions.InvalidArgumentException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

public class DAGRunner{

    public HgnDAG dag;

    public DAGRunner(HgnDAG dag){
        this.dag = dag;
    }

    private ArrayList<Map.Entry<Integer, Double>> getOverallResult(ArrayList<ArrayList<Map.Entry<Integer,Double>>> subnetResults, int returnNum){
        System.out.println("Network recall:");
        for(ArrayList<Map.Entry<Integer,Double>> subnet : subnetResults)
        {
            for(Map.Entry<Integer,Double> r : subnet)
            {
                System.out.print(r.getKey()+":"+r.getValue()+" ");
            }
            System.out.println();
        }

        ArrayList<Map.Entry<Integer, Double>> result = new ArrayList<>();

        HashMap<Integer,Double> memCount = new HashMap<>();

        for(ArrayList<Map.Entry<Integer,Double>> subnet : subnetResults)
        {
            for(Map.Entry<Integer,Double> r : subnet)
            {
                //if the mem have not been encountered yet
                if(memCount.getOrDefault(r.getKey(),null) == null)
                {
                    memCount.put(r.getKey(),r.getValue());
                }else //if it has been encountered
                {
                    memCount.put(r.getKey(),memCount.get(r.getKey())+r.getValue());
                }
            }
        }

        List<Map.Entry<Integer,Double>> sortList = new ArrayList<>(memCount.entrySet());
        Collections.sort(sortList, new Comparator<Map.Entry<Integer, Double>>() {
            @Override
            public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
                return (o2.getValue().compareTo(o1.getValue()));
            }
        });

        System.out.println("Top "+returnNum+": ");
        for(int i = 0; i < returnNum && i < sortList.size(); i++)
        {
            result.add(sortList.get(i));
            System.out.println(result.get(i).getKey()+":"+result.get(i).getValue());
        }

        return result;
    }

    public ArrayList<Map.Entry<Integer, Double>> run(int[] input,HGNComposition.HGNState state) {

        //split up raw input to feed into HGN compositions
        int inputSize = dag.nodes.get(0).hgnComposition.getNumBaseInputs();
        if(input.length % inputSize != 0)
            throw new IllegalArgumentException("Input array size did not divide into the number of HGN compositions");

        //run the base layer of the DAG
        ArrayList<ArrayList<Map.Entry<Integer,Double>>> subnetVotes = new ArrayList<>();
        ArrayList<HGNRunner.HGNResult> subnetResults = new ArrayList<>();
        for(int i=0; i < dag.nodes.size();i++)
        {
            int inputOffset = (i*inputSize);
            int inputOffsetEnd = (i*inputSize)+inputSize;
            subnetResults.add( dag.nodes.get(i).runner.run(Arrays.copyOfRange(input,inputOffset,inputOffsetEnd),state) );
            subnetVotes.add(subnetResults.get(i).votes);
        }

        //esemble inputs for top HGN composition
        int topInputSize = dag.top.hgnComposition.getNumBaseInputs();
        if(topInputSize != subnetResults.size() && topInputSize != subnetResults.size()+1)
            throw new IllegalArgumentException("Subnets did't produce enough results to feed next DAG Node");
        //boolean needsBuffer = dag.nodes.size()%2 == 0;
        //if(needsBuffer)
        //    topInputSize += 1;
        int[] topHGNInputs = new int[topInputSize];
        Arrays.fill(topHGNInputs,-1);

        for(int i=0; i < subnetResults.size(); i++)
        {
            topHGNInputs[i] = subnetResults.get(i).topResult;
            System.out.print(topHGNInputs[i]+" ");
        }
        System.out.println();

        HGNRunner.HGNResult topRes = dag.top.runner.run(topHGNInputs,state);
        for(Map.Entry<Integer,Double> e : topRes.votes)
        {
            System.out.print(e.getKey()+" ");
        }
        System.out.println();

        subnetVotes.add(topRes.votes);



        return getOverallResult(subnetVotes,3);
    }
}
