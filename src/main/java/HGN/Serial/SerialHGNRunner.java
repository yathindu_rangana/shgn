package HGN.Serial;

import HGN.*;
import Utilities.DebugUtils;
import java.util.AbstractMap;

import java.io.Serializable;
import java.util.*;

/**
 * SerialHGNRunner implments the run method from HGNRunner in a serial manner.
 */
public class SerialHGNRunner extends HGNRunner{

    public SerialHGNRunner(final int[] dims)
    {
        super(dims);
    }

    public SerialHGNRunner(HGNComposition hgn) {
        super(hgn);
    }

    private HashMap<Integer,Double> simpleCount(final ArrayList<ArrayList<Integer>> results)
    {
        HashMap<Integer,Double> count = new HashMap<>();

        for(ArrayList<Integer> layer : results)
        {
            for(Integer r : layer)
            {
                if(r != -1){
                    Double currentCount = count.getOrDefault(r, null);
                    if(currentCount == null){
                        count.put(r,1.0);
                    }else{
                        count.put(r,currentCount+1.0);
                    }
                }
            }
        }

        return count;
    }

    private HashMap<Integer,Double> scaledCount(final ArrayList<ArrayList<Integer>> results){
        HashMap<Integer,Double> count = new HashMap<>();

        double patternSize = 1;
        for(int j : hgn.getDiagnostic_layerSizes().get(0))
            patternSize *= j;

        //TODO: maybe shift into seperate function
        for(int b = 0; b < results.size(); b++)
        {
            ArrayList<Integer> l = results.get(b);

            int numNeurons = 1;
            for(int j : hgn.getDiagnostic_layerSizes().get(b))
                numNeurons *= j;

            for(Integer i : l)
            {
                if(i != -1) {
                    double val = (patternSize / numNeurons) / patternSize;
                    Double c = count.getOrDefault(i, null);
                    if (c == null) {
                        count.put(i, val);
                    } else {
                        count.put(i, c + val);
                    }
                }
            }
        }

        return count;
    }

    private ArrayList<Map.Entry<Integer,Double>> topK(final ArrayList<ArrayList<Integer>> results,int K)
    {
        ArrayList<Map.Entry<Integer,Double>> topK = new ArrayList<>();

        HashMap<Integer,Double> count = scaledCount(results);

        List<Map.Entry<Integer,Double>> sortList = new ArrayList<>(count.entrySet());
        Collections.sort(sortList, new Comparator<Map.Entry<Integer, Double>>() {
            @Override
            public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
                return (o2.getValue().compareTo(o1.getValue()));
            }
        });

        if(K >= sortList.size())
            K = sortList.size();

        Integer topNeuronResult = null;
        if(results.get(results.size()-1).get(0) != -1)
        {
            topK.add(new AbstractMap.SimpleEntry<Integer, Double>(results.get(results.size()-1).get(0),Double.MAX_VALUE));
            topNeuronResult = results.get(results.size()-1).get(0);
        }

        for(int i=0; i < K;i++)
        {
            //if(topNeuronResult != sortList.get(i).getKey())
                topK.add(sortList.get(i));
        }

        return topK;
    }


    @Override
    public HGNResult run(final int[] input) {

        ArrayList<ArrayList<Integer>> resultsPerLayer = new ArrayList<>(hgn.getLayers().size());
        int[] in = input.clone();
        for(int l = 0; l < hgn.getLayers().size(); l++)
        {
            ArrayList<GN> layer = hgn.getLayers().get(l);
            if(in.length != layer.size())
                throw new IllegalArgumentException("input size must match HGNComposition base layer size!");
            //input phase
            for(int i = 0; i < in.length; i++)
            {
                int sym = in[i];
                layer.get(i).setCurrentSymbol(sym);
            }
            //send phase
            sendToNeighbours(layer,hgn.getDiagnostic_layerSizes().get(l));
            //compute bias arrays & collect results
            resultsPerLayer.add(computeBiasArrays(layer));

            //send to upper neuron
            if((l+1) < hgn.getLayers().size()) {
                int[] temp = new int[hgn.getLayers().get(l+1).size()];
                final int[] currDim= hgn.getDiagnostic_layerSizes().get(l);
                final int[] newDim = hgn.getDiagnostic_layerSizes().get(l+1);
                int[] currCoord = new int[currDim.length];
                int[] newCoord = new int[newDim.length];

                for(int i=0;i<currCoord.length;i++)
                    currCoord[i]=0;

                for(int i = 0; i < temp.length;i++) {
                    for(int j=0;j<newCoord.length;j++)
                        newCoord[j]=currCoord[j]+1;

                    temp[i] = (char)resultsPerLayer.get(l).get(convertToIndex(newCoord,currDim)).intValue();
                    incrementCoord(currCoord,newDim);
                }
                in = temp;
            }

            for(GN n : layer)
                n.nullOutNeighbourSymbols();
        }

        DebugUtils.printHGN(resultsPerLayer);
        HGNResult result = new HGNResult(topK(resultsPerLayer,5),resultsPerLayer.get(resultsPerLayer.size()-1).get(0));
        return result;
    }

    private void sendToNeighbours(ArrayList<GN> layer, final int[] dims)
    {
        int[] currentCoord = new int[dims.length];
        for(int i = 0; i < currentCoord.length; i++)
            currentCoord[i] = 0;
        for(GN neuron : layer)
        {
            ArrayList<int[]> neighbourCoords = getNeighbours(currentCoord,dims);
            for(int d = 0; d < dims.length; d++)
            {
                int c = d*2;
                if(neighbourCoords.get(c) != null)
                    layer.get(convertToIndex(neighbourCoords.get(c),dims)).setNeighbourSymbol(c,neuron.getCurrentSymbol());

                c++;
                if(neighbourCoords.get(c) != null)
                    layer.get(convertToIndex(neighbourCoords.get(c),dims)).setNeighbourSymbol(c,neuron.getCurrentSymbol());
            }

            //increment coord column wise
            incrementCoord(currentCoord,dims);
        }
    }

    private ArrayList<Integer> computeBiasArrays(ArrayList<GN> layer)
    {
        ArrayList<Integer> results = new ArrayList<>();

        for(int i = 0; i < layer.size();i++)
        {
            GN neuron = layer.get(i);
            Integer key = neuron.computeCurrentKey();
            switch(hgn.getState())
            {
                case Store:
                    results.add(i,neuron.storeInBiasArray(key));
                    break;
                case Recall:
                    results.add(i,neuron.retreiveFromBiasArray(key));
                    break;
                default:
                    throw new IllegalStateException("HGNComposition state is invalid: "+hgn.getState());
            }
        }

        return results;
    }

    private void incrementCoord(int[] coord,final int[] dims)
    {
        boolean incrementDone = false;
        int incrementDimenion = 0;
        while(!incrementDone) {
            if (incrementDimenion < dims.length) {
                if (coord[incrementDimenion] < dims[incrementDimenion]) {
                    coord[incrementDimenion]++;
                    incrementDone = true;
                } else {
                    coord[incrementDimenion] = 0;
                    incrementDimenion++;
                }
            }else
            {
                incrementDone = true;
            }
        }
    }

}
