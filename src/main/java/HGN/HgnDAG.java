package HGN;

import HGN.Serial.SerialHGNRunner;

import java.io.IOException;
import java.io.Serializable;

import java.util.ArrayList;

public class HgnDAG implements Serializable{
    public class Node implements Serializable{
        public HGNComposition hgnComposition;
        public ArrayList<Node> nodes;
        public transient SerialHGNRunner runner;
        public Node(HGNComposition hgnComposition, ArrayList<Node> nodes)
        {
            this.hgnComposition = hgnComposition;
            this.nodes = nodes;
            this.runner = new SerialHGNRunner(this.hgnComposition);
        }

        private void readObject(java.io.ObjectInputStream in)
                throws IOException, ClassNotFoundException
        {
            in.defaultReadObject();

            this.runner = new SerialHGNRunner(this.hgnComposition);
        }

    }

    public Node top;
    public ArrayList<Node> nodes;

    private boolean topNeedsExtraVal;

    /**
     * HGN dags a usually made up of identical HGNCompositions, large networks can have massive bias arrays (~ 1gb mem)
     * Nodes with identical HGNs can share the HGN  referance. THIS MEANS THEY CAN NOT RUN IN PARALLEL TO EACH OTHER
     */
    public ArrayList<HGNComposition> discreteHGNs;

    public HgnDAG(final int dagBaseSize,int[] basePatternSize){
        nodes = new ArrayList<>(dagBaseSize);

        //if odd rememebr to add a buffer value to top's input
        topNeedsExtraVal = dagBaseSize%2 == 0;

        //TODO: HARD  coded for two layer dags
        discreteHGNs =  new ArrayList<>();
        for(int i = 0; i < dagBaseSize; i++)
            discreteHGNs.add(new HGNComposition(basePatternSize));

        for(int i = 0; i < dagBaseSize; i++)
        {
            //TODO: HARD  coded for two layer dags
            nodes.add(new Node(discreteHGNs.get(i),null));
        }
        int topInputSize = topNeedsExtraVal ? dagBaseSize+1 : dagBaseSize;
        //TODO: HARD  coded for two layer dags
        discreteHGNs.add(new HGNComposition(new int[]{topInputSize}));
        top = new Node(discreteHGNs.get(discreteHGNs.size()-1),nodes);
    }
}
