package HGN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 * GN represents one graph neuron.
 * A GN contains a bias array which stores the state of the neuron and it's immediate neighbours.
 */
public class GN implements Serializable {

    /**
     * The bias array store the state of the current neuron and it's neighbours for each unique pattern.
     * The key is the hash of `currentSymbol` of the current neuron and it's neighbours. See GN.computeCurrentKey for details
     * Each key is associated with an integer called the pattern Index. When a new bias array entry is added
     * the new index is current bias array size + 1.
     *
     */
    private HashMap<Integer,Integer> biasArray;
    //The current neurons symbol.
    private int currentSymbol;
    //Each of this neurons neighbours. The List's size is always number of dimensions * 2. Neurons on the edges will
    //just store a null value when no neighbour is available.
    private ArrayList<Integer> neighbourSymbols;

    public GN(int numDims)
    {
        biasArray = new HashMap<>();
        neighbourSymbols = new ArrayList<>();
        //initialize the arrayList with enough space for both neighbours from each dimension
        while(neighbourSymbols.size() < (numDims*2))
            neighbourSymbols.add(null);
    }

    /**
     * Adds the passed key to the bias array if it doesn't already exist
     * @param key
     * @return The index associated with the key.
     */
    public int storeInBiasArray(Integer key)
    {
        int existingKey = retreiveFromBiasArray(key);
        //if not in bias array
        if( existingKey== -1)
        {
            int val = this.biasArray.size()+1;
            this.biasArray.put(key,val);
            existingKey = val;
        }

        return existingKey;
    }

    /**
     *
     * @param key The key to use in the bias array entry
     * @return Index or null if key not in map
     */
    public int retreiveFromBiasArray(Integer key)
    {
        return biasArray.getOrDefault(key,-1);
    }

    public Integer computeCurrentKey()
    {
        Vector<Integer> s = new Vector<>(neighbourSymbols.size());
        s.add(this.currentSymbol);
        for(Integer i : this.neighbourSymbols)
        {
            s.add(i);
        }

        return s.hashCode();
    }

    public void nullOutNeighbourSymbols()
    {
        for(int i =0; i < neighbourSymbols.size();i++)
            neighbourSymbols.set(i,null);
    }

    public void setCurrentSymbol(int currentSymbol) {
        this.currentSymbol = currentSymbol;
    }

    public int getCurrentSymbol()
    {
        return currentSymbol;
    }

    public void setNeighbourSymbol(final int idx,final Integer symbol)
    {
        neighbourSymbols.set(idx,symbol);
    }



}

